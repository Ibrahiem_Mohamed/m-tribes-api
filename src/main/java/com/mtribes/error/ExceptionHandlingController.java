package com.mtribes.error;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.mongodb.MongoSocketException;

/**
 * @author Ibrahim AbdelHamid
 *
 *	Global error handling for any throw exceptions
 */
@ControllerAdvice
public class ExceptionHandlingController {
	
	    @ExceptionHandler(MongoSocketException.class)
	    public ResponseEntity<?> handleDatabaseErrorResponse() {
	    	return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	       
	    

}
