/**
 * @author Ibrahim AbdelHamid
 *
 *	Contains different error handling for different exceptions
 */
package com.mtribes.error;