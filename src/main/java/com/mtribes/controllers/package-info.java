/**
 * @author Ibrahim AbdelHamid
 *	
 *	This package contains all rest controller used by the App
 *	 with the associated endpoints
 */

package com.mtribes.controllers;
