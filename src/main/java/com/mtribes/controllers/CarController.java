package com.mtribes.controllers;

import com.mtribes.responses.CarsResponse;
import com.mtribes.services.CarService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Ibrahim AbdelHamid
 * 
 * CarController is a Rest controller with HTTP methods
 *  
 */

@RestController
@RequestMapping("/api")
@CrossOrigin("*")  // enable cross Origin for front-end
public class CarController {

	@Autowired
	CarService carService;

	/**
	 * Get Method for endpoint 'api/v1/cars' 
	 * 
	 * @return list of cars
	 */
	@GetMapping("/v1/cars")
	public ResponseEntity<CarsResponse> listCars() {

		// 1.create car response object which returned in ResponseEntity.
		CarsResponse response = new CarsResponse();

		// 2.set response from car list returned from car service.
		response.setPlacemarks(carService.getAllCars());
		

		// 3. return response with Http status 200.
		return new ResponseEntity<CarsResponse>(response, HttpStatus.OK);
	}
}
