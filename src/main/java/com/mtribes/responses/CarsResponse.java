package com.mtribes.responses;

import java.util.List;

import com.mtribes.models.Car;

/**
 * @author Ibrahim AbdelHamid
 *
 *	A DTO object used by CarController for returning a list of cars
 */
public class CarsResponse {

	private List<Car> placemarks;

	/**
	 * @return the placemarks
	 */
	public List<Car> getPlacemarks() {
		return placemarks;
	}

	/**
	 * @param placemarks the placemarks to set
	 */
	public void setPlacemarks(List<Car> placemarks) {
		this.placemarks = placemarks;
	}
	
	
}
