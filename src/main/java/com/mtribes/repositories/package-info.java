/**
 * @author Ibrahim AbdelHamid
 *	
 *	This package contains all DAO used by the App
 *	 for all different entities using Spring data interfaces 
 *	
 */
package com.mtribes.repositories;