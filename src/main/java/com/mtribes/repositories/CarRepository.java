package com.mtribes.repositories;

import com.mtribes.models.Car;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
/**
 * @author Ibrahim AbdelHamid
 * 
 * this interface is a spring data repository
 *  has DAO operations on the entity Car finding , saving , updating ..etc
 */

@Repository
public interface CarRepository extends MongoRepository<Car,String> {
}
