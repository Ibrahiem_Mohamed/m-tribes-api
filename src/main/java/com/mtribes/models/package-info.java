
/**
 * @author Ibrahim AbdelHamid
 *
 *	this Package hold all Entities which has a Equivalent 
 *	MongoDB collection of documents on the Database
 */
package com.mtribes.models;