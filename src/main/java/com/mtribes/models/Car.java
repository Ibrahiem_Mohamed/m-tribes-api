package com.mtribes.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mtribes.enums.EngineType;
import com.mtribes.enums.Exterior;
import com.mtribes.enums.Interior;

import java.util.List;

/**
 * @author Ibrahim AbdelHamid
 *
 *	Car Entity represents a mongoDB document
 *	used for containing car info that will be 
 *	presisted or retrived.
 *
 */
@Document
public class Car {

    @Id
    private String _id;
    private String address;
    private List<Double> coordinates;
    private EngineType engineType;
    private Exterior exterior;
    private int fuel;
    private Interior interior;
    private String name;
    private String vin;
	
    /**
	 * @return the _id
	 */
    @JsonIgnore
	public String get_id() {
		return _id;
	}
	/**
	 * @param _id the _id to set
	 */
	public void set_id(String _id) {
		this._id = _id;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the coordinates
	 */
	public List<Double> getCoordinates() {
		return coordinates;
	}
	/**
	 * @param coordinates the coordinates to set
	 */
	public void setCoordinates(List<Double> coordinates) {
		this.coordinates = coordinates;
	}
	/**
	 * @return the engineType
	 */
	public EngineType getEngineType() {
		return engineType;
	}
	/**
	 * @param engineType the engineType to set
	 */
	public void setEngineType(EngineType engineType) {
		this.engineType = engineType;
	}
	/**
	 * @return the exterior
	 */
	public Exterior getExterior() {
		return exterior;
	}
	/**
	 * @param exterior the exterior to set
	 */
	public void setExterior(Exterior exterior) {
		this.exterior = exterior;
	}
	/**
	 * @return the fuel
	 */
	public int getFuel() {
		return fuel;
	}
	/**
	 * @param fuel the fuel to set
	 */
	public void setFuel(int fuel) {
		this.fuel = fuel;
	}
	/**
	 * @return the interior
	 */
	public Interior getInterior() {
		return interior;
	}
	/**
	 * @param interior the interior to set
	 */
	public void setInterior(Interior interior) {
		this.interior = interior;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the vin
	 */
	public String getVin() {
		return vin;
	}
	/**
	 * @param vin the vin to set
	 */
	public void setVin(String vin) {
		this.vin = vin;
	}

  

}
