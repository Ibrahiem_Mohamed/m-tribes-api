package com.mtribes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MTribesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MTribesApplication.class, args);
	}

}

