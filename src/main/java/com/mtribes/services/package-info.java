/**
 * @author Ibrahim AbdelHamid
 * 
 * this package contains service layer used for 
 * handling logic between controllers, repositories and any util 
 *
 */
package com.mtribes.services;