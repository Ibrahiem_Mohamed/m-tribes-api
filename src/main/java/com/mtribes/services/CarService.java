package com.mtribes.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtribes.models.Car;
import com.mtribes.repositories.CarRepository;

/**
 * @author Ibrahim AbdelHamid
 * 
 * CarService Holding the logic and A separation layer
 * used by the CarController for getting list of cars 
 * injected by the carRepo Interface (DAO).
 *
 */
@Service
public class CarService {

	@Autowired
	CarRepository carRepository;

	
	/**
	 * Get All cars entities saved and persisted in DB.
	 * @return List<Car> 
	 */
	public List<Car> getAllCars() {
		return carRepository.findAll();
	}

}
