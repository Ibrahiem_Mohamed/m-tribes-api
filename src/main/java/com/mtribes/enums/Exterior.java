package com.mtribes.enums;

/**
 * @author Ibrahim AbdelHamid
 * 
 *	Enum Representing All different values used by
 *	 Car entity field exterior
 */
public enum Exterior {
	UNACCEPTABLE,
	GOOD;

}
