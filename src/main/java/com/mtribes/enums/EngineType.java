package com.mtribes.enums;

/**
 * @author Ibrahim AbdelHamid
 * 
 *	Enum Representing All different values used by
 *	 Car entity field engineType
 */
public enum EngineType {
	CE;
}
